import React from "react";
import axios from "axios";
import {
  DataGrid,
  Column,
  Editing,
  Scrolling,
  Lookup,
  Summary,
  TotalItem,
} from "devextreme-react/data-grid";
import { Button } from "devextreme-react/button";
import { SelectBox } from "devextreme-react/select-box";

import CustomStore from "devextreme/data/custom_store";
import { formatDate } from "devextreme/localization";
import "whatwg-fetch";

const URL = "http://localhost:58300/api";

const REFRESH_MODES = ["full", "reshape", "repaint"];

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      ordersData: new CustomStore({
        key: "id",
        load: () => axios.get(`${URL}/dcandidate`).then((res) => res),
        insert: (values) => axios.post(`${URL}/dcandidate`, values),
        update: (key, values) => {
          console.log(values);
          axios.put(`${URL}/dcandidate/` + key, values);
        },
        remove: (key) => axios.delete(`${URL}/dcandidate/` + key),
      }),

      requests: [],
      refreshMode: "reshape",
    };

    this.clearRequests = this.clearRequests.bind(this);
    this.handleRefreshModeChange = this.handleRefreshModeChange.bind(this);
  }

  clearRequests() {
    this.setState({ requests: [] });
  }

  handleRefreshModeChange(e) {
    this.setState({ refreshMode: e.value });
  }

  render() {
    const { refreshMode, ordersData, customersData, shippersData } = this.state;
    return (
      <React.Fragment>
        <DataGrid
          id="grid"
          showBorders={true}
          dataSource={ordersData}
          repaintChangesOnly={true}
        >
          <Editing
            refreshMode={refreshMode}
            mode="popup"
            allowAdding={true}
            allowDeleting={true}
            allowUpdating={true}
          />

          <Scrolling mode="virtual" />
        </DataGrid>
      </React.Fragment>
    );
  }
}

export default App;
